import re
import string
from collections import Counter, OrderedDict


def remove_not_letters(tokens):
    return [token.lower() for token in tokens if re.match(r"[a-zа-яїґ]\w*", token, re.IGNORECASE)]


def words_count(tokens):
    return OrderedDict(sorted(Counter(tokens).items()))


def lexical_variety(tokens):
    return len(set(tokens))/len(tokens)*100
