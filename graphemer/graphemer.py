import collections
import os
import re

from nltk import RegexpTokenizer

dir_path = os.path.dirname(os.path.realpath(__file__))

Token = collections.namedtuple('Token', 'value start end')

WORD_COMPOSING_LENGTH = 8


class Tokens(list):

    def __init__(self, text):
        # text = preliminary(original_text)
        # text = re.sub("[()\"\']", "", text)
        # tokens = re.split("[\s]*[.–,]*[\s]+|[\s]+[.–,]*[\s]*|\.", text)

        tokenizer = \
            RegexpTokenizer(r"[^,.;:!?\s«»\"]+(?:\.[^А-ЯҐЇA-Z\s])*[^,;.:!?\s«»\"]*|[,;.:!«»\"]")

        tokens = tokenizer.tokenize(text)

        super().__init__(indexized(text, tokens))

    def values(self):
        return [token.value for token in self]


class Sentences(list):

    def __init__(self, text):
        tokenizer = RegexpTokenizer(r".*?(?:\S{2,}|\s)(?:[.!?](?![йцукенгшщзххїґфівапролджєячсмитьбa-z])|$)")

        sentences = tokenizer.tokenize(text)

        with open(dir_path + "/dicts/contractions.txt") as f:
            for contraction in f.readlines():
                ss = []
                i = 0
                while i < len(sentences):
                    if i < len(sentences)-1 and sentences[i].endswith(contraction.rstrip("\n")):
                        ss.append(sentences[i] + sentences[i+1])
                        i += 1
                    else:
                        ss.append(sentences[i])
                    i += 1
                sentences = ss[:]

        sentences = [sentence.strip(" ") for sentence in sentences]
        super().__init__(indexized(text, sentences))


def compose_word(word, k=WORD_COMPOSING_LENGTH):
    if k == 0:
        return [word]

    res = []
    last_in_dict = 0
    i = 0
    while i <= len(word):
        if i+k > len(word):
            if word[last_in_dict:]:
                res += compose_word(word[last_in_dict:], k-1)
            break
        with open(dir_path + "/dicts/words.txt") as f:
            in_dict = False
            for line in f.readlines():
                if word[i:i+k] == line.rstrip("\n"):
                    in_dict = True
                    break
            if in_dict:
                if last_in_dict < i:
                    res += compose_word(word[last_in_dict:i], WORD_COMPOSING_LENGTH)
                    if i == len(word):
                        break
                res.append(word[i:i + k])
                i += k
                last_in_dict = i
                continue

            i += 1

    return res

# print(compose_word("аєвіродосталь"))
# print(compose_word("аєвіркіодостатріккк"))


def compose_letterspacing(text):
    for match in re.finditer("(?:^|\s)(\S(?:\s+\S){2,})(?:$|\s)", text):
        words = compose_word(match.group(1).replace(" ", ""))
        text = text[:match.start(1)] + " ".join(words) + " " + text[match.end(1)+1:]
    return text


def remove_non_textual(text):
    text = re.sub(r"<.*?>", "", text)
    text = re.sub(r"\s?\[.*?\]", "", text)
    text = re.sub(r"(http|ftp|https)://([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?", "", text)
    return text


def get_emails(text):
    return indexized(text, re.findall(r"[a-zA-Z\.-]+@[a-zA-Z\.-]+(?:\.[a-zA-Z]+)+", text))


def get_files(text):
    return indexized(text, re.findall(r"(?<=\b)(?<!@)[^@\s]+\.[a-zA-Z0-9]+", text))


def get_ners(text):

    ners = []
    with open(dir_path + "/dicts/ners.txt") as ner:
        for word in ner.readlines():
            word = word.rstrip("\n")
            for m in re.finditer(re.escape(word), text):
                ners.append((word, m.start()))

    ners = sorted(ners, key=lambda x: x[1])
    return indexized(text, [ner[0] for ner in ners])


def get_stop_words(words):
    with open(dir_path + "/dicts/stopwords.txt") as f:
        stopwords = [word.rstrip("\n") for word in f.readlines()]
        words = [word for word in words if word in stopwords]
    return words


def remove_stop_words(text):
    with open(dir_path + "/dicts/stopwords.txt") as f:
        stopwords = [word.rstrip("\n") for word in f.readlines()]
        for word in stopwords:
            text = re.sub(r"\b%s\b" % word, "", text, flags=re.IGNORECASE)
    return text


def indexized(text, tokens):

    text_length = len(text)
    res = []

    for token in tokens:
        diff = text_length - len(text)
        res.append(Token(token, text.index(token) + diff, text.index(token) + len(token) + diff))
        text = text[text.index(token) + len(token):]

    return res



