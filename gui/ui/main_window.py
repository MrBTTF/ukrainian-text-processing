from datetime import datetime

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import *
from ui.ui_main_window import Ui_MainWindow
import pyqtgraph as pg
import pyqtgraph.exporters

from graphemer import graphemer
from gui.ui.models import StemTableMovie
from lexer import lexer
from morpher.morpher import get_morph_info, translate_tag


def select_tokens(function):
    def wrapper(s):
        s.ui.textEdit.clear_selection()

        function(s)

        s.ui.textEdit.select_tokens(s.tokens_to_select)

        s.ui.listWidgetGrapheme.clear()
        for token in s.tokens_to_select:
            item = QListWidgetItem(token.value)
            item.setData(Qt.UserRole, (token.start, token.end))
            # item.setFlags(item.flags() & ~Qt.ItemIsEditable)
            s.ui.listWidgetGrapheme.addItem(item)

    return wrapper


class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.textEdit.setText((
            "Нахвалявся мороз усіх у лісі поморозити.Холодних вітрів, лютих хурделиць накликав." 
            "Завивали вітри, шаленіли хурделиці.Снігом усе замітали.Скрутно, голодно стало птахам і "
            "звірам.Навіть білочка й та засмутилася.Були в неї сякі-такі припаси, та вийшли.А до весни"
            "ще далеченько. «Добре їжакові, — думає білочка, —добре борсукові й ведмедеві: позасинали у"
            " своїх схованках під снігом і горя не знають.А тут, мабуть, доведеться, по чужих лісах "
            "поживи шукати». Дострибала білочка до узлісся.Аж чує: хтось шурх-шурх, рип-рип!"
            "Глянула, а то лісник на лижах пробирається.За плечима в нього тугий мішок, при боці — "
            "верболіз та осика, в пучечки пов'язані."
        ))

        self.editText_tokenize()
        self.tokens_to_select = []

        self.ui.tableView.setModel(StemTableMovie(self))
        self.ui.tableView.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)
        self.ui.tableView.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)
        self.ui.tableView.horizontalHeader().setSectionResizeMode(1, QHeaderView.Stretch)

        # self.ui.tableWidgetGrapheme.setColumnCount(1)
        # self.ui.tableWidgetGrapheme.setHorizontalHeaderLabels(["Токен"])
        # self.ui.tableWidgetGrapheme.verticalHeader().setVisible(False)
        self.ui.tableWidgetUnique.setColumnCount(2)
        self.ui.tableWidgetUnique.setHorizontalHeaderLabels(["Слово", "Кількість"])
        self.ui.tableWidgetUnique.verticalHeader().setVisible(False)

        self.ui.actionOpenFile.triggered.connect(self.actionOpenFile_triggered)
        self.ui.actionSaveAsFile.triggered.connect(self.actionSaveAsFile_triggered)

        self.ui.pushButtonSpacing.clicked.connect(self.pushButtonSpacing_clicked)
        self.ui.pushButtonNonText.clicked.connect(self.pushButtonNonText_clicked)
        self.ui.pushButtonStopWords.clicked.connect(self.pushButtonStopWords_clicked)

        self.ui.pushButtonTokenize.clicked.connect(self.pushButtonTokenize_clicked)
        self.ui.pushButtonSentences.clicked.connect(self.pushButtonSentences_clicked)
        self.ui.pushButtonEmail.clicked.connect(self.pushButtonEmail_clicked)
        self.ui.pushButtonFilenames.clicked.connect(self.pushButtonFilenames_clicked)
        self.ui.pushButtonNER.clicked.connect(self.pushButtonNER_clicked)

        self.ui.pushButtonLexAnalyze.clicked.connect(self.pushButtonLexAnalyze_clicked)
        self.ui.pushButtonPlot.clicked.connect(self.pushButtonPlot_clicked)
        self.ui.pushButtonSavePlot.clicked.connect(self.pushButtonSavePlot_clicked)

        self.ui.textEdit.clicked.connect(self.textEdit_clicked)
        self.ui.textEdit.clear_selection()

        self.ui.listWidgetGrapheme.itemClicked.connect(self.listWidgetGrapheme_itemClicked)

    def _show_morph_info(self, token):
        self.ui.tableView.model().clear()

        self.ui.labelWord.setText("<b>%s</b>" % token)

        for w in get_morph_info(token):
            self.ui.tableView.model().addRow(w.lemma,
                                             w.stem,
                                             translate_tag(w.tag.POS or ''),
                                             translate_tag(w.tag.case or ''),
                                             translate_tag(w.tag.gender or ''),
                                             translate_tag(w.tag.number or ('однина' if w.tag.gender else '')),
                                             translate_tag(w.tag.animacy or ''),
                                             translate_tag(w.tag.tense or ''),
                                             translate_tag(w.tag.person or ''),
                                             translate_tag(w.tag.aspect or ''),
                                             )

    def actionOpenFile_triggered(self):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.AnyFile)
        dlg.setNameFilters(["Текстові файли (*.txt)", "Усі (*.*)"])

        if dlg.exec_():
            filename = dlg.selectedFiles()[0]
            with open(filename, 'r') as f:
                data = f.read()
                self.ui.textEdit.setText(data)

    def actionSaveAsFile_triggered(self):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.AnyFile)
        dlg.setAcceptMode(QFileDialog.AcceptSave)
        dlg.setNameFilters(["Текстові файли (*.txt)", "Усі (*.*)"])
        if dlg.exec_():
            filename = dlg.selectedFiles()[0]
            with open(filename, 'w+') as f:
                f.write(self.ui.textEdit.toPlainText())

    def textEdit_clicked(self):
        tc = self.ui.textEdit.textCursor()

        token = None
        for i, token in enumerate(self.tokens):
            if token.start <= tc.position() <= token.end:
                self.ui.textEdit.highlight_range(token.start, token.end)
                self.ui.listWidgetGrapheme.setCurrentRow(i)
                break

        if not token:
            return

        self._show_morph_info(token.value)

    def listWidgetGrapheme_itemClicked(self, item):
        self._show_morph_info(item.text())
        data = item.data(Qt.UserRole)
        self.ui.textEdit.highlight_range(data[0], data[1])

    def pushButtonSpacing_clicked(self):
        self.ui.textEdit.setText(graphemer.compose_letterspacing(self.ui.textEdit.toPlainText()))

    def pushButtonNonText_clicked(self):
        self.ui.textEdit.setText(graphemer.remove_non_textual(self.ui.textEdit.toPlainText()))

    def pushButtonStopWords_clicked(self):
        self.ui.textEdit.setText(graphemer.remove_stop_words(self.ui.textEdit.toPlainText()))

    @select_tokens
    def editText_tokenize(self):
        self.tokens = graphemer.Tokens(self.ui.textEdit.toPlainText())
        self.tokens_to_select = self.tokens

    def pushButtonTokenize_clicked(self):
        self.editText_tokenize()

    @select_tokens
    def pushButtonSentences_clicked(self):
        self.tokens_to_select = graphemer.Sentences(self.ui.textEdit.toPlainText())

    @select_tokens
    def pushButtonEmail_clicked(self):
        self.tokens_to_select = graphemer.get_emails(self.ui.textEdit.toPlainText())

    @select_tokens
    def pushButtonFilenames_clicked(self):
        self.tokens_to_select = graphemer.get_files(self.ui.textEdit.toPlainText())

    @select_tokens
    def pushButtonNER_clicked(self):
        self.tokens_to_select = graphemer.get_ners(self.ui.textEdit.toPlainText())

    def pushButtonLexAnalyze_clicked(self):
        self.ui.pushButtonPlot.setEnabled(True)
        self.ui.pushButtonSavePlot.setEnabled(True)

        self.editText_tokenize()
        tokens = lexer.remove_not_letters(self.tokens.values())
        self.unique_tokens = []
        self.unique_counts = []

        self.ui.tableWidgetUnique.setRowCount(len(set(tokens)))
        for i, (token, count) in enumerate(lexer.words_count(tokens).items()):
            self.ui.tableWidgetUnique.setItem(i, 0, QTableWidgetItem(token))
            self.ui.tableWidgetUnique.setItem(i, 1, QTableWidgetItem(str(count)))
            self.unique_tokens.append(token)
            self.unique_counts.append(count)

        self.ui.labelLexVar.setText(str(lexer.lexical_variety(tokens)) + "%")

    def pushButtonPlot_clicked(self):
        self.plt = pg.plot([i for i, _ in enumerate(self.unique_tokens)], self.unique_counts)

    def pushButtonSavePlot_clicked(self):
        exporter = pg.exporters.ImageExporter(self.plt.plotItem)

        # exporter.params.param('width').setValue(1920, blockSignal=exporter.widthChanged)
        # exporter.params.param('height').setValue(1080, blockSignal=exporter.heightChanged)
        exporter.parameters()['width'] = 1920

        # save to file
        exporter.export('plot%s.png' % datetime.now())





