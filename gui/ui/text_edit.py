from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QColor, QTextCharFormat, QTextCursor
from PyQt5.QtWidgets import QTextEdit

BLANK_COLOR = QColor("#FFFFFF")
SELECT_COLOR = QColor("#B5E9AA")
HIGHLIGHT_COLOR = QColor("#61BD4F")


class TextEdit(QTextEdit):

    clicked = pyqtSignal()

    def __init__(self, parent):
        super().__init__(parent)
        self.setAcceptRichText(False)
        self.cursors = []
        self.highlighted_token = None

    def mouseReleaseEvent(self, event):
        self.clicked.emit()

    def clear_selection(self):
        self.highlighted_token = None

        fmt = QTextCharFormat()
        fmt.setBackground(QColor(255, 255, 255))

        cursor = QTextCursor(self.document())
        cursor.movePosition(QTextCursor.End, QTextCursor.KeepAnchor)
        cursor.setCharFormat(fmt)
        #
        # for id, tokens in self.cursors[:]:
        #     if id == cursor_id:
        #         self.cursors.remove((id, tokens))
        #         continue
        #     self.select_tokens(tokens)

    def highlight_range(self, start, end, color=HIGHLIGHT_COLOR):
        if self.highlighted_token:
            s, e, prev_color = self.highlighted_token
            self.highlighted_token = None
            self.highlight_range(s, e, prev_color)

        cursor = QTextCursor(self.document())
        cursor.setPosition(start, QTextCursor.MoveAnchor)
        cursor.setPosition(end, QTextCursor.KeepAnchor)

        self.highlighted_token = (start, end, cursor.charFormat().background().color())

        fmt = QTextCharFormat()
        fmt.setBackground(color)
        cursor.setCharFormat(fmt)

    def select_tokens(self, tokens):
        if not tokens:
            return

        cursor = QTextCursor(self.document())

        fmt = QTextCharFormat()
        fmt.setBackground(SELECT_COLOR)

        for token in tokens:
            cursor.setPosition(token.start, QTextCursor.MoveAnchor)
            cursor.setPosition(token.end, QTextCursor.KeepAnchor)
            cursor.setCharFormat(fmt)




