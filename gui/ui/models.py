from PyQt5.QtCore import QModelIndex, QAbstractTableModel, QVariant, Qt


class StemTableMovie(QAbstractTableModel):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.__data = []
        self.__columnNames = [
            "Лема", "Стем",
            "Частина мови",
            "Відмінок",
            "Рід",
            "Число",
            "Іст./неіст.",
            "Час",
            "Особа",
            "Вид",
        ]

    def addRow(self, *row_value, row=None):
        if row is None:
            row = self.rowCount() - 1
        row += 1
        self.beginInsertRows(QModelIndex(), row, row)
        self.__data.insert(row, list(map(str, row_value)))
        self.endInsertRows()

    def rowCount(self, parent=None, *args, **kwargs):
        return len(self.__data)

    def columnCount(self, parent=None, *args, **kwargs):
        return len(self.__columnNames)

    def data(self, index, role=None):
        if not index.isValid() or role != Qt.DisplayRole:
            return QVariant()

        cell = self.__data[index.row()][index.column()]
        return cell or ''

    def headerData(self, col, orientation, role=None):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.__columnNames[col]

        return QVariant()

    def flags(self, index):
        return Qt.ItemIsEnabled

    def clear(self):
        self.beginResetModel()
        self.__data = []
        self.endResetModel()


