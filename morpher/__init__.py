import os
from pymorphy2.opencorpora_dict import storage

storage.GRAPHEME_PATH = os.path.dirname(os.path.realpath(__file__)) + "/grammemes.json"
