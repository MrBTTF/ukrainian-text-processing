import re

import pymorphy2
import pymorphy2_dicts_uk

morph = pymorphy2.MorphAnalyzer(path=pymorphy2_dicts_uk.get_path())


PERFECTIVEGERUND = re.compile("в?ши(?:с[ьяи])?$")

REFLEXIVE = re.compile("(с[яь])$")

ADJECTIVE = re.compile("("
                       "[иі]й|[еє]є?|[ая]я?|ії?|"
                       "ого|ої|[иі]х|"
                       "ому|ій|[иі]м|"
                       "ого|[еє]є?|[ую]ю?|[иі]х|"
                       "[иі]м|о[юй]|[іи]ми|"
                       "ім|ому|ій|[іи]х"
                       ")$")

PARTICIPLE = re.compile("(уч|юч|ач|яч|л|т|[аеє]н|і?ш)$")

THEMES = re.compile("(ну|і)$")

VERB = re.compile("("
                  "у|ю|" 
                  "[еєиї]ш|" 
                  "е|є|[иї]ть|" 
                  "[єе]мо?|" 
                  "[еєиї]те|" 
                  "[уюая]ть|" 

                  "ло|в|г|c|"

                  "(?:[ую]ва|ота|ну|і|и|а)?ти|"
                  "но|то"
                  ")$")

NOUN = re.compile("("
                  "а|я|и|і|ї|у|[оеє]ю|о|е|є|"
                  "ій|ок|[ая]ми?|[ая]х|"
                  "[еоєя]м|[оеє]ві|ю|"
                  "[ії]в|ми|(?<=[аеиоуюяіїє][^й])"
                  ")$")

RVRE = re.compile("^(.*?[аеиоуюяіїє])(.*)$")


DERIVATIONAL = re.compile("((?<=[^аеиоуюяіїє])ь?н|(?:ес)?еньк|і?сть)?$")


class Word:

    def __init__(self, form, lemma, stem, tag):
        self.form = form
        self.lemma = lemma
        self.stem = stem
        self.tag = tag

    def equals(self, word):
        return self.lemma == word.lemma \
               and self.tag.POS == word.tag.POS \
               and self.tag.gender == word.tag.gender \
               and self.tag.tense == word.tag.tense \
               and self.tag.aspect == word.tag.aspect


def stem_without_dict(word):
    word = word.lower()

    result = []

    rvre = RVRE.match(word)
    if not rvre.group(2):
        return [[word, None]]

    start, rv = rvre.group(1), rvre.group(2)

    if PERFECTIVEGERUND.search(rv):
        result.append([PERFECTIVEGERUND.sub("", rv), "perf gerund"])

    rv = REFLEXIVE.sub("", rv)
    if ADJECTIVE.search(rv):
        adj = ADJECTIVE.sub("", rv)
        if PARTICIPLE.search(adj):
            result.append([PARTICIPLE.sub("", adj), "part"])
        else:
            result.append([ADJECTIVE.sub("", adj), "adj"])

    if VERB.search(rv):
        result.append([VERB.sub("", rv), "verb"])
    if NOUN.search(rv):
        result.append([NOUN.sub("", rv), "noun"])

    for i, s in enumerate(result):
        if s[0].endswith("і"):
            s[0] = s[0][:-1]

        rv2 = RVRE.match(s[0])
        if not rv2 or (rv2 and not rv2.group(2)):
            result[i][0] = start + s[0]
            result[i] = tuple(result[i])
            continue

        s[0] = DERIVATIONAL.sub("", s[0])

        if len(s[0]) > 1 and s[0][-1] == s[0][-2]:
            s[0] = s[0][:-1]
        elif s[0].endswith("ь"):
            s[0] = s[0][:-1]
        elif s[0].endswith("ат") or s[0].endswith("ен"):
            s[0] = s[0][:-2]

        result[i][0] = start + s[0]
        result[i] = tuple(result[i])

    return result


def compare_letters(a, b):
    return a == b \
           or a == 'о' != b in "еі" \
           or b == 'о' != a in "еі" \
           or a == 'г' != b == 'ж' \
           or a == 'к' != b == 'ч' \
           or a == 'х   ' != b == 'ш' \



def longest_match(string1, string2):
    match = ""

    m = re.match(RVRE, string1)
    if not m:
        return string1

    start, rv1 = m.group(1), m.group(2)
    m = re.match(RVRE, string2)
    rv2 = m.group(2)

    for i in range(min(len(rv1), len(rv2))):
        if compare_letters(rv1[i], rv2[i]):
            match += rv2[i]
        else:
            break

    if not match:
        return string1

    return start + match


def get_morph_info(word):
    word = word.lower()

    res = []
    for p in morph.parse(word):
        wrd = Word(word, p.normal_form, word, p.tag)

        if p.tag.POS in [None, "ADVB", "GRND", "PREP", "CONJ", "PRCL", "INTJ"]:
            res.append(wrd)
            continue

        lexemes = [lexeme for lexeme in p.lexeme if p.tag.tense == lexeme.tag.tense]
        similar = [longest_match(p.normal_form, lexeme.word) for lexeme in lexemes]
        shortest = min(set(similar), key=len)
        if shortest[-1] == shortest[-2] or shortest[-1] == 'ь':
            shortest = shortest[:-1]

        wrd.stem = shortest

        if not any([wrd.equals(w) for w in res]):

            res.append(Word(word, p.normal_form, shortest, p.tag))

    return res


def translate_tag(tag):
    return morph.lat2cyr(tag)

# examples = [
#     # "ти",
#     "весна",
#     "міський",
#     "підводний",
#     "підводного",
#     "швидкий",
#     "малесенький",
#     "бігати",
#     "повільне",
#     "безпритульними",
#     "критикувати",
#     "блиск",
#     "бюро",
#     "тут",
#     "танцюючи",
#     "восени",
#     "авіадиспетчер",
#     "налив",
#     "наливши",
#     "село",
#     "етапом",
# ]
#
#
#
# for word in examples:
#     print(word, get_stems(word))


# ss = get_stems("іє")
# print(ss)

