import os

from graphemer import graphemer

dir_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

def make_words(length):
    with open(dir_path + "/tools/words/words_raw.txt") as full:
        with open(dir_path + "/graphemer/dicts/words.txt", "w+") as words:
            for line in full.readlines():
                if 1 < len(line.rstrip("\n")) <= length \
                        or (len(line.rstrip("\n")) == 1
                            and line.rstrip("\n") in ['у', 'в', 'і', 'й', 'з',
                                                      'о', 'а', 'є', 'я']):
                    words.write(line.lower())

make_words(graphemer.WORD_COMPOSING_LENGTH)
