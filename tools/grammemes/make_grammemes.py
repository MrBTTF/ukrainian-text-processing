import json
import os

dir_path = os.path.dirname(os.path.realpath(__file__))

data = json.load(open(dir_path + "/grammemes_original.json"))
for gr in data:
    gr[2],  gr[3] = gr[3],  gr[2]

with open("../../morpher/grammemes.json", 'w') as outfile:
    json.dump(data, outfile, ensure_ascii=False)

