import re

regex = re.compile(r"^(?:\S+\.\s*)+$")

with open("contractions_raw.txt") as full:
    with open("../../grapheme/dicts/contractions.txt", "w") as contractions:
        for line in full.readlines():
            line = line.rstrip("\n")
            if len(line[:-1]) > 1 and re.search(regex, line):
                contractions.write(line.lower() + '\n')
