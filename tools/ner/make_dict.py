import glob


lines = []

with open("../../grapheme/dicts/ners.txt", "w") as ner:
    for filename in glob.glob('data/*.ann'):
        if ".tok" not in filename:
            with open(filename) as f:
                lines += [line.split("\t")[-1] for line in f.readlines()]

    for line in set(lines):
        ner.write(line.split("\t")[-1])
